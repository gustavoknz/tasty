package com.kieling.tasty.service;

import com.kieling.tasty.model.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RecipesService {
    @GET("baking.json")
    Call<List<Recipe>> getAllRecipes();
}
