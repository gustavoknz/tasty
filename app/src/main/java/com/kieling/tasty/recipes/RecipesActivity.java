package com.kieling.tasty.recipes;

import android.appwidget.AppWidgetManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.kieling.tasty.MyDatabase;
import com.kieling.tasty.R;
import com.kieling.tasty.model.Recipe;
import com.kieling.tasty.model.ingredient.Ingredient;
import com.kieling.tasty.model.ingredient.IngredientDao;
import com.kieling.tasty.model.ingredient.IngredientEntity;
import com.kieling.tasty.service.RecipesService;
import com.kieling.tasty.steps.IngredientsStepsListActivity;
import com.kieling.tasty.utils.Constants;
import com.kieling.tasty.widget.CollectionAppWidgetProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecipesActivity extends AppCompatActivity {
    private static final String TAG = "RecipesActivity";
    private RecipesAdapter mAdapter;
    private RecipesViewModel mViewModel;
    private RecipesService mService;
    private RecyclerView mRecyclerView;
    private TextView mButtonRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        Log.d(TAG, "Initializing...");

        mService = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build()
                .create(RecipesService.class);

        mButtonRetry = findViewById(R.id.recipes_empty_view);
        mViewModel = ViewModelProviders.of(this).get(RecipesViewModel.class);
        if (mViewModel.recipes == null || mViewModel.recipes.isEmpty()) {
            fetchData();
        } else {
            Log.d(TAG, "No need to fetch data!");
        }

        mRecyclerView = findViewById(R.id.recipes_recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapter = new RecipesAdapter(mViewModel.recipes, new RecipesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Recipe recipe) {
                //Update widget
                updateWidget(recipe);

                //Call activity to show that recipe
                Intent intent = new Intent(getApplicationContext(), IngredientsStepsListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.PARAM_RECIPE_KEY, recipe);
                intent.putExtras(bundle);
                startActivity(intent);
            }

            private void updateWidget(Recipe recipe) {
                final List<IngredientEntity> ingredients = new ArrayList<>(recipe.getIngredients().size());
                for (Ingredient ingredient : recipe.getIngredients()) {
                    ingredients.add(new IngredientEntity(ingredient.getQuantity(), ingredient.getMeasure(), ingredient.getIngredient()));
                }
                Log.d(TAG, "Updating widget..........");
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Deleting entries...");
                        IngredientDao ingredientsDao = MyDatabase
                                .getInstance(getApplicationContext())
                                .ingredientsDao();
                        ingredientsDao.deleteAllIngredients();
                        Log.d(TAG, "Inserting entries... " + ingredients);
                        ingredientsDao.insertIngredients(ingredients);
                    }
                });
                Log.d(TAG, "Getting widget...");
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(getApplicationContext(), CollectionAppWidgetProvider.class));
                Log.d(TAG, "appWidgetIds: " + Arrays.toString(appWidgetIds));

                //Trigger data update to handle the GridView widgets and force a data refresh
                Log.d(TAG, "Notifying App Widget View Data Changed...");
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widget_list_view);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private void fetchData() {
        Log.d(TAG, "Fetching data...");
        mViewModel.recipes = new ArrayList<>();
        Call<List<Recipe>> call = mService.getAllRecipes();
        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response) {
                List<Recipe> recipes = response.body();
                Log.d(TAG, "Got response code = " + response.code());
                if (response.isSuccessful() && response.code() == 200 && recipes != null && !recipes.isEmpty()) {
                    Log.d(TAG, "Got response: " + recipes.size() + " recipes");
                    mViewModel.recipes.clear();
                    mViewModel.recipes.addAll(recipes);
                    mAdapter.notifyDataSetChanged();
                    hideRetryButton();
                } else {
                    Log.i(TAG, "Could not fetch data");
                    showRetryButton();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t) {
                Log.e(TAG, "Got failure throwable", t);
                Log.d(TAG, "Got failure: " + call);
                showRetryButton();
            }
        });
    }

    private void showRetryButton() {
        Log.d(TAG, "Showing retry");
        mRecyclerView.setVisibility(View.GONE);
        mButtonRetry.setVisibility(View.VISIBLE);
        mButtonRetry.setClickable(true);
    }

    private void hideRetryButton() {
        Log.d(TAG, "Hiding retry");
        mButtonRetry.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }
}
