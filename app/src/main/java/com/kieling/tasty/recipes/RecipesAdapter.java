package com.kieling.tasty.recipes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kieling.tasty.R;
import com.kieling.tasty.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;

class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.RecipesViewHolder> {
    private List<Recipe> mDataSet;
    private OnItemClickListener mListener;

    RecipesAdapter(List<Recipe> dataSet, OnItemClickListener listener) {
        mDataSet = dataSet;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecipesAdapter.RecipesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe_view, parent, false);
        return new RecipesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesViewHolder holder, int position) {
        final Recipe recipe = mDataSet.get(position);
        if (!recipe.getImage().isEmpty()) {
            Picasso.get().load(recipe.getImage()).into(holder.mImageView);
        }
        holder.mImageView.setContentDescription(recipe.getName());
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(recipe);
            }
        });
        holder.mTextView.setText(recipe.getName());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    interface OnItemClickListener {
        void onItemClick(Recipe recipe);
    }

    static class RecipesViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;
        TextView mTextView;

        RecipesViewHolder(View view) {
            super(view);
            mImageView = view.findViewById(R.id.recipe_item_image);
            mTextView = view.findViewById(R.id.recipe_item_title);
        }
    }
}
