package com.kieling.tasty.recipes;

import android.arch.lifecycle.ViewModel;

import com.kieling.tasty.model.Recipe;

import java.util.List;

public class RecipesViewModel extends ViewModel {
    public List<Recipe> recipes;
}
