package com.kieling.tasty.model.ingredient;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "ingredients")
public class IngredientEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "quantity")
    public float quantity;
    @ColumnInfo(name = "measure")
    public String measure;
    @ColumnInfo(name = "name")
    public String ingredient;

    public IngredientEntity(float quantity, String measure, String ingredient) {
        this.quantity = quantity;
        this.measure = measure;
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "IngredientEntity{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", measure='" + measure + '\'' +
                ", ingredient='" + ingredient + '\'' +
                '}';
    }
}
