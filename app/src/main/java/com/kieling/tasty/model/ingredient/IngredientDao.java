package com.kieling.tasty.model.ingredient;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface IngredientDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIngredients(List<IngredientEntity> ingredients);

    @Query("SELECT * FROM ingredients")
    List<IngredientEntity> loadIngredients();

    @Query("DELETE FROM ingredients")
    void deleteAllIngredients();
}
