package com.kieling.tasty.steps;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.kieling.tasty.R;
import com.kieling.tasty.utils.Constants;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link IngredientsStepsListActivity}.
 */
public class StepDetailActivity extends AppCompatActivity {
    private static final String TAG = "StepDetailActivity";
    private int mStepToShow = 0;
    private StepDetailsViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        mViewModel = ViewModelProviders.of(this).get(StepDetailsViewModel.class);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null || bundle.getParcelableArrayList(Constants.PARAM_ALL_STEPS_KEY) == null) {
            Log.d(TAG, "Got bundle = " + bundle);
        } else {
            mStepToShow = bundle.getInt(Constants.PARAM_STEP_TO_SHOW_KEY, 0);
            mViewModel.steps = bundle.getParcelableArrayList(Constants.PARAM_ALL_STEPS_KEY);
            Log.d(TAG, "Got steps " + mViewModel.steps);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putParcelableArrayList(Constants.PARAM_ALL_STEPS_KEY, mViewModel.steps);
            arguments.putInt(Constants.PARAM_STEP_TO_SHOW_KEY, mStepToShow);
            Log.d(TAG, "Parameter to fragment: " + mViewModel.steps.get(mStepToShow));
            StepDetailFragment fragment = new StepDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.step_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }
}
