package com.kieling.tasty.steps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kieling.tasty.R;
import com.kieling.tasty.model.Recipe;
import com.kieling.tasty.model.Step;
import com.kieling.tasty.model.ingredient.Ingredient;
import com.kieling.tasty.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link StepDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class IngredientsStepsListActivity extends AppCompatActivity {
    private static final String TAG = "IngredStepsListActivity";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients_steps_list);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (findViewById(R.id.step_detail_container) != null) {
            // The detail container view will be present only in the large-screen layouts (res/values-w900dp).
            // If this view is present, then the activity should be in two-pane mode.
            mTwoPane = true;
        }
        Log.d(TAG, "mTwoPane = " + mTwoPane);
        Bundle bundle = getIntent().getExtras();
        if (bundle == null || bundle.getParcelable(Constants.PARAM_RECIPE_KEY) == null) {
            Log.e(TAG, "Got bundle = " + bundle);
        } else {
            Recipe recipe = bundle.getParcelable(Constants.PARAM_RECIPE_KEY);
            if (recipe == null) {
                Log.e(TAG, "Weird. My recipe is NULL");
            } else {
                Log.d(TAG, "Got recipe " + recipe.getName());
                setupLists(recipe);
                if (actionBar != null) {
                    actionBar.setTitle(recipe.getName());
                }
            }
        }
    }

    private void setupLists(Recipe recipe) {
        RecyclerView ingredientsRecyclerView = findViewById(R.id.item_ingredients_list);
        ingredientsRecyclerView.setAdapter(new ItemIngredientsRecyclerViewAdapter(
                getApplicationContext(), recipe.getIngredients()));
        ingredientsRecyclerView.setHasFixedSize(true);
        RecyclerView stepsRecyclerView = findViewById(R.id.item_step_list);
        stepsRecyclerView.setAdapter(new ItemStepsRecyclerViewAdapter(this, new ArrayList<>(recipe.getSteps()), mTwoPane));
        stepsRecyclerView.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    public static class ItemIngredientsRecyclerViewAdapter
            extends RecyclerView.Adapter<ItemIngredientsRecyclerViewAdapter.ViewHolder> {
        private final List<Ingredient> mIngredients;
        private final Context mContext;

        ItemIngredientsRecyclerViewAdapter(Context context, List<Ingredient> ingredients) {
            mContext = context;
            mIngredients = ingredients;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ingredient_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            Ingredient ingredient = mIngredients.get(position);
            if (ingredient.getQuantity() % 1.0 == 0) {
                holder.mContentView.setText(mContext.getString(R.string.item_ingredient_content_integer,
                        ingredient.getQuantity(), ingredient.getMeasure(), ingredient.getIngredient()));
            } else {
                holder.mContentView.setText(mContext.getString(R.string.item_ingredient_content_decimal,
                        ingredient.getQuantity(), ingredient.getMeasure(), ingredient.getIngredient()));
            }
            holder.itemView.setTag(ingredient);
        }

        @Override
        public int getItemCount() {
            return mIngredients.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mContentView = view.findViewById(R.id.item_ingredient_content);
            }
        }
    }

    public static class ItemStepsRecyclerViewAdapter
            extends RecyclerView.Adapter<ItemStepsRecyclerViewAdapter.ViewHolder> {
        private final IngredientsStepsListActivity mParentActivity;
        private final ArrayList<Step> mSteps;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                Bundle arguments = new Bundle();
                arguments.putParcelableArrayList(Constants.PARAM_ALL_STEPS_KEY, mSteps);
                arguments.putInt(Constants.PARAM_STEP_TO_SHOW_KEY, position);
                Log.d(TAG, "Parameter to StepDetailActivity steps.size: " + mSteps.size() + "; position: " + position);
                if (mTwoPane) {
                    StepDetailFragment fragment = new StepDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.step_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, StepDetailActivity.class);
                    intent.putExtras(arguments);
                    context.startActivity(intent);
                }
            }
        };

        ItemStepsRecyclerViewAdapter(IngredientsStepsListActivity parent, ArrayList<Step> steps, boolean twoPane) {
            mParentActivity = parent;
            mSteps = steps;
            mTwoPane = twoPane;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_step_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            holder.mContentView.setText(mSteps.get(position).getShortDescription());

            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mSteps.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mContentView = view.findViewById(R.id.item_step_content);
            }
        }
    }
}
