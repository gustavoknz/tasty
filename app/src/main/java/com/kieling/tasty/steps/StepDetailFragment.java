package com.kieling.tasty.steps;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.kieling.tasty.R;
import com.kieling.tasty.model.Step;
import com.kieling.tasty.utils.Constants;

import java.util.List;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link IngredientsStepsListActivity}
 * in two-pane mode (on tablets) or a {@link StepDetailActivity}
 * on handsets.
 */
public class StepDetailFragment extends Fragment implements View.OnClickListener {
    private static final String KEY_PLAY_WHEN_READY = "play_when_ready";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String TAG = "StepDetailFragment";

    private PlayerView mPlayerView;
    private SimpleExoPlayer mPlayer;

    private DataSource.Factory mMediaDataSourceFactory;
    private boolean mShouldAutoPlay;

    private ImageView mIvHideControllerButton;
    private boolean mPlayWhenReady;
    private int mCurrentWindow;
    private long mPlaybackPosition;

    private List<Step> mSteps;
    private int mCurrentStep;
    private TextView mPreviousButton;
    private TextView mNextButton;
    private TextView mDescriptionView;
    private View mNoVideoView;

    public StepDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSteps = getArguments().getParcelableArrayList(Constants.PARAM_ALL_STEPS_KEY);
        mCurrentStep = getArguments().getInt(Constants.PARAM_STEP_TO_SHOW_KEY, 0);
        Log.i(TAG, "Received step '" + mSteps + "'");
        if (!mSteps.get(mCurrentStep).getVideoURL().isEmpty()) {
            if (savedInstanceState == null) {
                mPlayWhenReady = true;
                mCurrentWindow = 0;
                mPlaybackPosition = 0;
            } else {
                mPlayWhenReady = savedInstanceState.getBoolean(KEY_PLAY_WHEN_READY);
                mCurrentWindow = savedInstanceState.getInt(KEY_WINDOW);
                mPlaybackPosition = savedInstanceState.getLong(KEY_POSITION);
            }

            mShouldAutoPlay = true;
            mMediaDataSourceFactory = new DefaultDataSourceFactory(getContext(),
                    Util.getUserAgent(getContext(), getString(R.string.app_name)), null);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.step_detail, container, false);
        mDescriptionView = rootView.findViewById(R.id.step_detail_description);
        if (mSteps != null) {
            mNoVideoView = rootView.findViewById(R.id.step_detail_no_video_view);
            mPlayerView = rootView.findViewById(R.id.step_detail_player_view);
            mIvHideControllerButton = rootView.findViewById(R.id.exo_controller);
            mPreviousButton = rootView.findViewById(R.id.step_detail_container_button_previous);
            mNextButton = rootView.findViewById(R.id.step_detail_container_button_next);
            mPreviousButton.setOnClickListener(this);
            mNextButton.setOnClickListener(this);
            updateButtons();
        } else {
            Log.e(TAG, "Weird! Received step = null");
        }
        return rootView;
    }

    private void updateButtons() {
        if (mCurrentStep <= 0) {
            mPreviousButton.setTextColor(getResources().getColor(android.R.color.secondary_text_light_nodisable));
            mPreviousButton.setClickable(false);
        } else {
            mPreviousButton.setTextColor(getResources().getColor(android.R.color.white));
            mPreviousButton.setClickable(true);
        }
        if (mCurrentStep + 1 >= mSteps.size()) {
            mNextButton.setTextColor(getResources().getColor(android.R.color.secondary_text_light_nodisable));
            mNextButton.setClickable(false);
        } else {
            mNextButton.setTextColor(getResources().getColor(android.R.color.white));
            mNextButton.setClickable(true);
        }
    }

    private void initializeViews() {
        mDescriptionView.setText(mSteps.get(mCurrentStep).getDescription());
        if (mSteps.get(mCurrentStep).getVideoURL().isEmpty()) {
            mNoVideoView.setVisibility(View.VISIBLE);
            mPlayerView.setVisibility(View.GONE);
        } else {
            mNoVideoView.setVisibility(View.GONE);
            mPlayerView.setVisibility(View.VISIBLE);
        }

        mPlayerView.requestFocus();
        mPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), new DefaultTrackSelector());
        mPlayerView.setPlayer(mPlayer);
        mPlayer.setPlayWhenReady(mShouldAutoPlay);
        Log.d(TAG, "Setting video URL: " + mSteps.get(mCurrentStep).getVideoURL());
        MediaSource mediaSource = new ExtractorMediaSource.Factory(mMediaDataSourceFactory)
                .createMediaSource(Uri.parse(mSteps.get(mCurrentStep).getVideoURL()));

        boolean haveStartPosition = mCurrentWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            mPlayer.seekTo(mCurrentWindow, mPlaybackPosition);
        }

        mPlayer.prepare(mediaSource, !haveStartPosition, false);
        mIvHideControllerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayerView.hideController();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23 && !mSteps.get(mCurrentStep).getVideoURL().isEmpty()) {
            initializeViews();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || mPlayer == null) && !mSteps.get(mCurrentStep).getVideoURL().isEmpty()) {
            initializeViews();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (mPlayer != null) {
            updateStartPosition();
            mShouldAutoPlay = mPlayer.getPlayWhenReady();
            mPlayer.release();
            mPlayer = null;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        updateStartPosition();

        outState.putBoolean(KEY_PLAY_WHEN_READY, mPlayWhenReady);
        outState.putInt(KEY_WINDOW, mCurrentWindow);
        outState.putLong(KEY_POSITION, mPlaybackPosition);
        super.onSaveInstanceState(outState);
    }

    private void updateStartPosition() {
        mPlaybackPosition = mPlayer.getCurrentPosition();
        mCurrentWindow = mPlayer.getCurrentWindowIndex();
        mPlayWhenReady = mPlayer.getPlayWhenReady();
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "View clicked: " + v.getId());
        switch (v.getId()) {
            case R.id.step_detail_container_button_previous:
                Log.d(TAG, "Previous clicked. current: " + mCurrentStep);
                if (mCurrentStep > 0) {
                    --mCurrentStep;
                    releasePlayer();
                    updateButtons();
                    initializeViews();
                }
                break;
            case R.id.step_detail_container_button_next:
                Log.d(TAG, "Next clicked. current: " + mCurrentStep);
                if (mCurrentStep + 1 <= mSteps.size()) {
                    ++mCurrentStep;
                    releasePlayer();
                    updateButtons();
                    initializeViews();
                }
                break;
        }
    }
}
