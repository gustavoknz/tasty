package com.kieling.tasty.utils;

public class Constants {
    public static String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/";
    public static final String PARAM_RECIPE_KEY = "recipeKey";
    public static final String PARAM_ALL_STEPS_KEY = "allStepKey";
    public static final String PARAM_STEP_TO_SHOW_KEY = "stepToShowKey";
}
