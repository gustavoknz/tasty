package com.kieling.tasty;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.util.Log;

import com.kieling.tasty.model.ingredient.IngredientDao;
import com.kieling.tasty.model.ingredient.IngredientEntity;

@Database(entities = {IngredientEntity.class}, version = 1, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    private static final String TAG = "MoviesDatabase";
    private static final String DATABASE_NAME = "tasty.db";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static MyDatabase sInstance;

    public static MyDatabase getInstance(Context context) {
        Log.d(TAG, "Getting the database");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        MyDatabase.class, MyDatabase.DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build();
                Log.d(TAG, "New database built");
            }
        }
        return sInstance;
    }

    public abstract IngredientDao ingredientsDao();
}
