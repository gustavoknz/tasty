package com.kieling.tasty.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import com.kieling.tasty.R;
import com.kieling.tasty.recipes.RecipesActivity;

public class CollectionAppWidgetProvider extends AppWidgetProvider {
    private static final String TAG = "CollAppWidgetProvider";
    public static final String ACTION_WIDGET_CLICKED = "actionWidgetClicked";

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        Log.d(TAG, "in onReceived. action: " + intent.getAction());
        if (ACTION_WIDGET_CLICKED.equals(intent.getAction())) {
            Intent recipesIntent = new Intent(context, RecipesActivity.class);
            recipesIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(recipesIntent);
            Log.d(TAG, "Starting RecipesActivity...");
        } else {
            int appWidgetIds[] = mgr.getAppWidgetIds(new ComponentName(context, CollectionAppWidgetProvider.class));
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widget_list_view);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // Perform this loop procedure for each App Widget that belongs to this provider
        Log.d(TAG, "in onUpdate...");
        for (int appWidgetId : appWidgetIds) {
            // Create an Intent to launch MainActivity when clicked
            Log.d(TAG, "appWidgetId: " + appWidgetId);

            // Construct the RemoteViews object
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.layout_widget_simple);

            /* **** */
            Intent toastIntent = new Intent(context, CollectionAppWidgetProvider.class);
            toastIntent.setAction(ACTION_WIDGET_CLICKED);
            toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(R.id.widget_list_view, toastPendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, rv);
            /* **** */

            // Widgets allow click handlers to only launch pending intents
            //Intent appIntent = new Intent(context, RecipesActivity.class);
            //rv.setOnClickFillInIntent(R.id.item_widget_text, appIntent);

            Intent intent = new Intent(context, WidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            Log.d(TAG, "views.getLayoutId(): " + rv.getLayoutId());
            rv.setRemoteAdapter(R.id.widget_list_view, intent);

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, rv);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }
}
