package com.kieling.tasty.widget;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.kieling.tasty.MyDatabase;
import com.kieling.tasty.R;
import com.kieling.tasty.model.ingredient.Ingredient;
import com.kieling.tasty.model.ingredient.IngredientEntity;

import java.util.ArrayList;
import java.util.List;

public class WidgetService extends RemoteViewsService {
    private static final String TAG = "WidgetService";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        Log.d(TAG, "in onGetViewFactory");
        return new MyWidgetRemoteViewsFactory(this.getApplicationContext());
    }
}

class MyWidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private static final String TAG = "MyWidgetRemoteViewsFact";
    private final Context mContext;
    private ArrayList<Ingredient> mIngredientsList = new ArrayList<>();

    MyWidgetRemoteViewsFactory(Context context) {
        Log.d(TAG, "MyWidgetRemoteViewsFactory constructor");
        this.mContext = context;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "in onCreate");
        mIngredientsList = new ArrayList<>();
    }

    @Override
    public void onDataSetChanged() {
        Log.d(TAG, "in onDataSetChanged");
        mIngredientsList = new ArrayList<>();
        List<IngredientEntity> entries = MyDatabase.getInstance(mContext)
                .ingredientsDao()
                .loadIngredients();
        if (entries == null) {
            Log.d(TAG, "Got no entries at all");
        } else {
            Log.d(TAG, "Got " + entries.size() + " entries");
            for (IngredientEntity ie : entries) {
                mIngredientsList.add(new Ingredient(ie.quantity, ie.measure, ie.ingredient));
            }
        }
    }

    /*
     * Similar to getView of Adapter where instead of View
     * we return RemoteViews
     */
    @Override
    public RemoteViews getViewAt(int position) {
        Log.d(TAG, "in getViewAt");
        if (position == AdapterView.INVALID_POSITION) {
            return null;
        }
        Log.d(TAG, "list: " + mIngredientsList);
        RemoteViews remoteView = new RemoteViews(mContext.getPackageName(), R.layout.item_widget);
        if (mIngredientsList != null && mIngredientsList.size() > 0) {
            Ingredient ingredient = mIngredientsList.get(position);
            Log.d(TAG, "Showing ingredient " + ingredient);
            String text = mContext.getString(R.string.item_ingredient_content_integer,
                    ingredient.getQuantity(), ingredient.getMeasure(), ingredient.getIngredient());
            remoteView.setTextViewText(R.id.item_widget_text, text);
            remoteView.setOnClickFillInIntent(R.id.item_widget_text, new Intent());
        }
        return remoteView;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "in getCount");
        return mIngredientsList == null ? 0 : mIngredientsList.size();
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "in getItemId. position: " + position);
        return position;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "in onDestroy");
        mIngredientsList = null;
    }

    @Override
    public boolean hasStableIds() {
        Log.d(TAG, "in hasStableIds");
        return true;
    }

    @Override
    public RemoteViews getLoadingView() {
        Log.d(TAG, "in getLoadingView");
        return null;
    }

    @Override
    public int getViewTypeCount() {
        Log.d(TAG, "in getViewTypeCount");
        return 1;
    }
}
