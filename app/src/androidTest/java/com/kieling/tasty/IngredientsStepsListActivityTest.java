package com.kieling.tasty;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;

import com.kieling.tasty.model.ingredient.Ingredient;
import com.kieling.tasty.model.Recipe;
import com.kieling.tasty.model.Step;
import com.kieling.tasty.steps.IngredientsStepsListActivity;
import com.kieling.tasty.utils.Constants;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class IngredientsStepsListActivityTest extends InstrumentationTestCase {
    @Rule
    public ActivityTestRule<IngredientsStepsListActivity> mActivityRule =
            new ActivityTestRule<>(IngredientsStepsListActivity.class, true, false);

    @Test
    public void testIngredientsAreShown() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.PARAM_RECIPE_KEY, buildRecipe());
        intent.putExtras(bundle);
        mActivityRule.launchActivity(intent);

        //onView(withId(R.id.step_detail_container)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.item_ingredients_list)).check(new RecyclerViewItemCountAssertion(3));
        onView(withId(R.id.item_step_list)).check(new RecyclerViewItemCountAssertion(2));
    }

    private Parcelable buildRecipe() {
        ArrayList<Ingredient> ingredients = new ArrayList<>(3);
        ingredients.add(new Ingredient(1.1F, "kg", "Powder 0"));
        ingredients.add(new Ingredient(2.2F, "kg", "Powder 1"));
        ingredients.add(new Ingredient(3.3F, "kg", "Powder 2"));

        ArrayList<Step> steps = new ArrayList<>(3);
        steps.add(new Step(0, "First step", "This a the first step of this recipe", "http://blah1.com", ""));
        steps.add(new Step(1, "Second step", "This a the second step of this recipe", "http://blah2.com", ""));
        return new Recipe(5678, "My recipe", ingredients, steps, 4, "");
    }
}
