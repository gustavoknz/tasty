package com.kieling.tasty;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;

import com.kieling.tasty.recipes.RecipesActivity;
import com.kieling.tasty.utils.Constants;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class RecipesInstrumentedTest extends InstrumentationTestCase {
    @Rule
    public ActivityTestRule<RecipesActivity> mActivityRule =
            new ActivityTestRule<>(RecipesActivity.class, true, false);
    private MockWebServer server;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        server = new MockWebServer();
        server.start();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        Constants.BASE_URL = server.url("/").toString();
    }

    @Test
    public void testRecipesAreShown() {
        String serverResponse = RestServiceTestHelper.getMockedServerResponseSuccess();
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(serverResponse));

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.recipes_empty_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.recipes_recycler_view)).check(new RecyclerViewItemCountAssertion(4));
        onView(withId(R.id.recipes_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

    @Test
    public void testEmptyTextIsShownWhenError() {
        server.enqueue(new MockResponse()
                .setResponseCode(404)
                .setBody(RestServiceTestHelper.getMockedServerResponseFail()));

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.recipes_empty_view)).check(matches(isDisplayed()));
        onView(withText(R.string.recipes_empty_text)).check(matches(isDisplayed()));
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.kieling.tasty", appContext.getPackageName());
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
        server = null;
        super.tearDown();
    }
}
